﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using HipChatLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HipChatLibTest
{
    [TestClass]
    public class TestHipChatLib
    {
        string me = @"0laGdlm154ohGFQy0L9FxEcmqLORGGrKT1dGMphM";

        [TestMethod]
        public void TestUserByTokenAuthenticate()
        {
            var client = new HipChatLib.HipChatClient(me);
            var task = client.AuthenticateAsync();
            task.Wait();

            Assert.IsTrue(task.Result);
        }

        //[TestMethod]
        public async void TestUserAuthenticate()
        {
            var authId = "";
            var authSecret = "";

            var username = "";
            var password = "";

            var client = new HipChatLib.HipChatClient(authId, authSecret, username, password);

            Assert.IsTrue(await client.AuthenticateAsync());
        }

        [TestMethod]
        public void TestGetAllRooms()
        {
            var client = new HipChatLib.HipChatClient(me);
            var task = client.GetAllRoomsAsync();
            task.Wait();

            Assert.IsTrue(task.Result.items.Any());
        }

        [TestMethod]
        public void TestGetRoom()
        {
            var client = new HipChatLib.HipChatClient(me);
            var task = client.GetRoomAsync("361610");
            task.Wait();

            Assert.IsTrue(task.Result != null);
        }

        [TestMethod]
        public void TestGetRoomHistory()
        {
            var client = new HipChatLib.HipChatClient(me);
            var task = client.GetRoomHistoryAsync("361610");
            task.Wait();

            Assert.IsTrue(task.Result.items.Any());

            foreach (var item in task.Result.items)
                Assert.IsTrue(item.Author != string.Empty);
        }

        [TestMethod]
        public void TestGetUserData()
        {
            var client = new HipChatLib.HipChatClient(me);
            var task = client.GetUserAsync("567850");
            task.Wait();

            Assert.IsTrue(!string.IsNullOrEmpty(task.Result.name));
        }

        [TestMethod]
        public void TestPostComment()
        {
            var client = new HipChatLib.HipChatClient(me);
            var task = client.PostCommentAsync("361610", "testing at " + DateTime.Now.ToLongTimeString(), "red");
            task.Wait();

            Assert.IsTrue(task.Result);
        }
    }
}
