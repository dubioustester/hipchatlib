﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using HipChatLib.Model;
using Newtonsoft.Json;

namespace HipChatLib
{
    public class HipChatClient
    {
        private readonly string personalToken;

        private readonly object authId;
        private readonly string authSecret;
        private readonly string username;
        private readonly string password;

        private const string OAUTH_URL = @"https://api.hipchat.com/v2/oauth/token";
        private const string TOKEN_AUTH_URL = @"https://api.hipchat.com/v2/room?auth_token={0}&auth_test=true";
        private const string ROOMS_URL = @"https://api.hipchat.com/v2/room?auth_token={0}";
        private const string ROOM_BY_ID_URL = @"https://api.hipchat.com/v2/room/{1}?auth_token={0}";
        private const string ROOM_HISTORY_URL = @"https://api.hipchat.com/v2/room/{1}/history?auth_token={0}";
        private const string USER_BY_ID_URL = @"https://api.hipchat.com/v2/user/{1}?auth_token={0}";
        private const string NOTIFY_ROOM = @"https://api.hipchat.com/v2/room/{0}/notification";

        public HipChatClient(string personalToken)
        {
            this.personalToken = personalToken;
        }

        public HipChatClient(object authId, string authSecret, string username, string password)
        {
            this.authId = authId;
            this.authSecret = authSecret;
            this.username = username;
            this.password = password;
        }

        public async Task<bool> AuthenticateAsync()
        {
            try
            {
                var httpClient = new HttpClient();
                var response = await httpClient.GetStringAsync(string.Format(TOKEN_AUTH_URL, personalToken));
                var result = JsonConvert.DeserializeObject<AuthResponse>(response);
                return result.success != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<RoomsResponse> GetAllRoomsAsync()
        {
            var httpClient = new HttpClient();
            var requestUri = string.Format(ROOMS_URL, personalToken);
            var response = await httpClient.GetStringAsync(requestUri);
            var result = JsonConvert.DeserializeObject<RoomsResponse>(response);
            return result;
        }

        public async Task<RoomResponse> GetRoomAsync(string id)
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetStringAsync(string.Format(ROOM_BY_ID_URL, personalToken, id));
            var result = JsonConvert.DeserializeObject<RoomResponse>(response);
            return result;
        }

        public async Task<RoomHistoryResponse> GetRoomHistoryAsync(string id)
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetStringAsync(string.Format(ROOM_HISTORY_URL, personalToken, id));
            var result = JsonConvert.DeserializeObject<RoomHistoryResponse>(response);
            return result;
        }

        public async Task<UserResponse> GetUserAsync(string id)
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetStringAsync(string.Format(USER_BY_ID_URL, personalToken, id));
            var result = JsonConvert.DeserializeObject<UserResponse>(response);
            return result;
        }

        public async Task<bool> PostCommentAsync(string roomId, string message, string color = null)
        {
            try
            {
                var request = new NotifyRoomRequest
                {
                    message = message
                };

                var jsonRequest = JsonConvert.SerializeObject(request);
                
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", personalToken);
                
                var requestUri = string.Format(NOTIFY_ROOM, roomId);
                var response = await httpClient.PostAsync(requestUri, new StringContent(jsonRequest, Encoding.UTF8, "application/json"));

                return response.StatusCode == HttpStatusCode.NoContent;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
