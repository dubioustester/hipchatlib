﻿using System;
using HipChatLib.Model;

namespace HipChatLib
{
    public class RoomResponse
    {
        public DateTime created { get; set; }
        public string guest_access_url { get; set; }
        public int id { get; set; }
        public bool is_archived { get; set; }
        public DateTime last_active { get; set; }
        public Links links { get; set; }
        public string name { get; set; }
        public User owner { get; set; }
        public User[] participants { get; set; }
        public string privacy { get; set; }
        public string topic { get; set; }
        public string xmpp_jid { get; set; }
    }
}