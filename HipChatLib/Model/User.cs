﻿using HipChatLib.Model;

namespace HipChatLib
{
    public class User
    {
        public int id { get; set; }
        public Links links { get; set; }
        public string name { get; set; }
    }
}