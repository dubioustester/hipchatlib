﻿using System;
using HipChatLib.Model;

namespace HipChatLib
{
    public class UserResponse : User
    {
        public DateTime created { get; set; }
        public string email { get; set; }
        public Group group { get; set; }
        public bool is_deleted { get; set; }
        public bool is_group_admin { get; set; }
        public bool is_guest { get; set; }
        public object last_active { get; set; }
        public string mention_name { get; set; }
        public object photo_url { get; set; }
        public object presence { get; set; }
        public string timezone { get; set; }
        public string title { get; set; }
        public string xmpp_jid { get; set; }
    }

    public class Group
    {
        public int id { get; set; }
        public Links links { get; set; }
    }

}